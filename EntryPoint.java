import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import org.json.JSONArray;
import org.json.JSONObject;

public class EntryPoint {
    private static final String CACHE_DIR = "./cache/";

    private static final String PAPERCLIP_JAR_FILENAME_TEMPLATE = "paper_%s.jar";
    private static final String PAPERCLIP_JAR_MAIN_CLASS = "io.papermc.paperclip.Main";

    private static final String PAPERMC_API_BUILDS_URL_TEMPLATE = "https://api.papermc.io/v2/projects/paper/versions/%s/builds";
    private static final String PAPERMC_API_DOWNLOAD_URL_TEMPLATE = "https://api.papermc.io/v2/projects/paper/versions/%s/builds/%d/downloads/paper-%s-%d.jar";

    public static void main(String[] args) throws Exception, IOException {
        System.out.println("Initializing");

        File cacheDir = new File(CACHE_DIR);

        final String minecraftVersion = System.getenv("MINECRAFT_VERSION");
        final String paperclipJarFilename = String.format(PAPERCLIP_JAR_FILENAME_TEMPLATE, minecraftVersion);

        final File paperclipJarFile = new File(CACHE_DIR + paperclipJarFilename);

        if (!paperclipJarFile.exists()) {
            if (!cacheDir.exists())
                cacheDir.mkdir();

            final String buildsJson = getHttpDocument(String.format(PAPERMC_API_BUILDS_URL_TEMPLATE, minecraftVersion));
            final Integer buildNumber = parseLatestBuildNumber(buildsJson);

            final String downloadUrl = String.format(PAPERMC_API_DOWNLOAD_URL_TEMPLATE, minecraftVersion, buildNumber, minecraftVersion, buildNumber);

            System.out.println("Downloading " + paperclipJarFilename + String.format(" (build %d)", buildNumber));
            downloadFile(downloadUrl, paperclipJarFile);
        }

        final String minecraftEula = System.getenv("MINECRAFT_EULA");
        System.setProperty("com.mojang.eula.agree", minecraftEula);

        runJarFile(paperclipJarFile, PAPERCLIP_JAR_MAIN_CLASS);
    }

    private static void runJarFile(File jarFile, String className) throws Exception {
        URL jarUrl = jarFile.toURI().toURL();

        try (URLClassLoader classLoader = new URLClassLoader(new URL[] { jarUrl })) {
            Class<?> mainClass = classLoader.loadClass(className);
            Method mainMethod = mainClass.getMethod("main", String[].class);

            System.out.println(String.format("Starting %s", className));
            mainMethod.invoke(null, (Object) new String[] {});
        }
    }

    private static String getHttpDocument(String urlString) throws IOException {
        try (InputStream in = new URL(urlString).openStream()) {
            return new String(in.readAllBytes());
        }
    }

    private static Integer parseLatestBuildNumber(String json) {
        JSONArray builds = new JSONObject(json).getJSONArray("builds");
        JSONObject latestBuild = builds.getJSONObject(builds.length() - 1);

        return latestBuild.getInt("build");
    }

    private static void downloadFile(String urlString, File destination) throws IOException {
        try (InputStream in = new URL(urlString).openStream();
                FileOutputStream out = new FileOutputStream(destination)) {
            in.transferTo(out);
        }
    }
}
