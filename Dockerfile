FROM eclipse-temurin:21-jdk-alpine AS builder

RUN mkdir /tmp/tmp

RUN apk add --no-cache \
    eudev libstdc++

RUN jlink --compress zip-9 --no-header-files --no-man-pages --strip-debug \
    --add-modules java.base,java.se,jdk.crypto.cryptoki,jdk.security.auth,jdk.unsupported,jdk.zipfs \
    --output /opt/java-minimal

WORKDIR /opt/app
ADD https://repo1.maven.org/maven2/org/json/json/20240303/json-20240303.jar json.jar
ADD EntryPoint.java ./
RUN javac -cp json.jar EntryPoint.java

FROM scratch

COPY --from=builder /tmp/tmp/ /tmp/

COPY --from=builder /opt/java-minimal/ /opt/java/

# Required by Java.
COPY --from=builder /lib/ld-musl-*.so.1 /lib/

# Utilized by OSHI (system information library).
COPY --from=builder /usr/lib/libudev.so.1 /usr/lib/

# Utilized by spark (profiler).
COPY --from=builder /usr/lib/libgcc_s.so.1 /usr/lib/libstdc++.so.6 /usr/lib/

ENV PATH="/opt/java/bin:$PATH"
ENV CLASSPATH="/opt/app/:/opt/app/json.jar"

ENV MINECRAFT_EULA=false
ENV MINECRAFT_VERSION=1.21

WORKDIR /data
VOLUME /data
EXPOSE 25565

COPY --from=builder /opt/app/ /opt/app/

ENTRYPOINT ["java", "EntryPoint"]
