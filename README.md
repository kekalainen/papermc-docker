# PaperMC Docker
A lightweight Java 21 -based [PaperMC](https://papermc.io/) Minecraft server container.

## Running
You can run the server using the following command.
```sh
docker run -it -d --name papermc -e MINECRAFT_VERSION=1.21 -e MINECRAFT_EULA=false -p 25565:25565 kekalainen/papermc:latest
```

To start the server you'll need to signify your agreement to [Minecraft EULA](https://account.mojang.com/documents/minecraft_eula) by setting the `MINECRAFT_EULA` environment variable to `true`. The `MINECRAFT_VERSION` environment variable can be used to switch between different versions of Minecraft (which are downloaded during the first startup).

## Data persistence
A volume for the server files is created automatically. Alternatively it's possible to map the container's `/data` directory to one on the host machine using the `-v /path/on/host:/data` flag.